<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <title>Document</title>
</head>

<body>
    <div class="main">
        <div class="wrapper">
            <h2 class="heading">Trắc nghiệm</h2>
            <div class="question-wrapper">
                <div class="page page-1">
                    <h3>Page 1/2</h3>
                    <div class="question-list">

                    </div>
                    <div class="next"><span>Tiếp theo</span></div>
                </div>
                <div class="page page-2" style="display: none;">
                    <h3>Page 2/2</h3>
                    <div class="question-list">

                    </div>
                    <div class="page-btn">
                        <div class="back">Quay lại trang trước</div>
                        <div class="submit-btn">Nộp bài</div>
                    </div>
                </div>
            </div>
            <div id="result" style="display:none">
            </div>
        </div>

    </div>
    <script>
        const quiz = [{
                question: "con heo có mấy cái móng ?",
                choices: [
                    "1",
                    "2",
                    "3",
                    "4",
                ],
                answer: "4"
            },
            {
                question: "con heo có mấy chân ?",
                choices: [
                    "4",
                    "2",
                    "1",
                    "3",
                ],
                answer: "1"
            },
            {
                question: "bảng chữ cái tiếng ang có bao nhiêu chữ",
                choices: [
                    "26",
                    "27",
                    "28",
                    "29",
                ],
                answer: "1"
            },
            {
                question: "bảng chữ cái tiếng việt có bao nhiêu chữ ?",
                choices: [
                    "27",
                    "29",
                    "26",
                    "28",
                ],
                answer: "2"
            },
            {
                question: "Bác Hồ quê ở đâu",
                choices: [
                    "Hầ Lội",
                    "Nghệ An",
                    "Sài Gòn",
                    "Đà Nẵng",
                ],
                answer: "2"
            },

            {
                question: "1 năm có bao nhiêu ngày ?",
                choices: [
                    "354",
                    "358",
                    "356",
                    "365",
                ],
                answer: "4"
            },
            {
                question: "tháng 8 có bao nhiêu ngày ?",
                choices: [
                    "22",
                    "31",
                    "44",
                    "17",
                ],
                answer: "2"
            },
            {
                question: "nhà T5 có mấy tầng",
                choices: [
                    "6",
                    "7",
                    "2",
                    "1",
                ],
                answer: "2"
            },
            {
                question: "một tỷ gồm mấy số 0 ?",
                choices: [
                    "9 chữ số",
                    "10 chữ số",
                    "8 chữ số",
                    "12 chữ số",
                ],
                answer: "1"
            },
            {
                question: "999 nhân 999 bằng bao nhiêu ?",
                choices: [
                    "999999",
                    "1000000",
                    "990810",
                    "998001 ",
                ],
                answer: "4"
            },

        ]
        renderQuestion(quiz);

        var nextPageBtn = document.querySelector('.next');
        var backBtn = document.querySelector('.back');
        var submitBtn = document.querySelector('.submit-btn');
        var page1 = document.querySelector('.page-1');
        var page2 = document.querySelector('.page-2');
        var answers = document.getElementsByName("answer")
        var answerForms = document.querySelectorAll('.answerForm');
        var result = document.getElementById('result');
        var score = 0;
        const badScore = "Bạn quá kém, cần ôn tập thêm"
        const mediumScore = "Cũng bình thường"
        const goodScore = "Sắp sửa làm được trợ giảng lớp PHP"

        console.log(4142132314)

        function checkResult(score) {
            if (score < 4) {
                result.innerHTML += '<span class="comment" style="color:#dc3545 ;">' + badScore + '</span>';
            } else if (score >= 4 && score <= 7) {
                result.innerHTML += '<span class="comment" style="color: #ffc107;">' + mediumScore + '</span>';
            } else if (score > 7) {
                result.innerHTML += '<span class="comment" style="color:#28a745 ;">' + goodScore + '</span>';
            }
            document.querySelector('#result').innerHTML += '<a href="/index.php" class="btn-retry">Phải bạn không ? Cùng sửa nhé :))</a>'
        }
        answerForms.forEach((answerForm, index) => {
            // console.log(answerForm.elements)
            let answer = answerForm.elements.length

            answerForm.onchange = function() {
                // 4142132314
                for (let i = 0; i < answerForm.elements.length; i++) {
                    const element = answerForm.elements[i];
                    var userChoice = Number(element.value) + 1;

                    if (element.checked == true) {
                        if (userChoice === Number(quiz[index].answer)) {
                            score++;
                            console.log(score)
                        } else {

                            console.log('Wrong answer')
                        }
                    } else {}

                }
            }
        })

        function renderPage(i, data, index) {
            var page = document.querySelector(`.page-${i} .question-list`);
            var ask = ` <div class="ask">
                            <p class="ask-text">
                                <span class="question-order">Câu hỏi ${index+1}: ${data.question }</span>
                                
                            </p>
                        </div>`
            var answers = ``
            data.choices.forEach((choice, num) => {
                answers += `<div class="input-group">
                                    <input type="radio" name="answer" value="${num}" id="answer_1">
                                    <label class="form-label">${choice}</label>
                                </div>`
            })
            page.innerHTML += `<div class="question">` +
                ask +
                `<div class="answer">
                                        <form action="" class="answerForm">` +
                answers +
                `</form>
                                    </div>
                                </div>`

        }

        function renderQuestion(quiz) {
            quiz.forEach((question, index) => {

                if (index <= 4) {
                    renderPage(1, question, index)
                } else renderPage(2, question, index)

            })
        }

        submitBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'none';
            result.style.display = 'flex'
            result.innerHTML = "Score: " + score + "/" + quiz.length + "</br>";
            checkResult(score)
        }


        nextPageBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'block';
            console.log('to page 2')
        }
        backBtn.onclick = function() {
            page2.style.display = 'none';
            page1.style.display = 'block';
        }
    </script>
</body>

</html>